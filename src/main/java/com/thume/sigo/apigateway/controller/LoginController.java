package com.thume.sigo.apigateway.controller;

import com.thume.sigo.apigateway.bean.auth.AuthResponse;
import com.thume.sigo.apigateway.bean.auth.LoginRequest;
import com.thume.sigo.apigateway.security.JwtTokenProvider;
import com.thume.sigo.apigateway.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/auth")
public class LoginController {

    @Autowired
    private ILoginService iLoginService;

    @CrossOrigin("*")
    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest loginRequest) {
        String token = iLoginService.login(loginRequest.getUsername(), loginRequest.getPassword());
        HttpHeaders headers = new HttpHeaders();
        List<String> headerlist = new ArrayList<>();
        List<String> exposeList = new ArrayList<>();
        headerlist.add("Content-Type");
        headerlist.add(" Accept");
        headerlist.add("X-Requested-With");
        headerlist.add("Authorization");
        headers.setAccessControlAllowHeaders(headerlist);
        exposeList.add("Authorization");
        headers.setAccessControlExposeHeaders(exposeList);
        headers.set("Authorization", token);
        return new ResponseEntity<AuthResponse>(new AuthResponse(token), headers, HttpStatus.CREATED);
    }

    @CrossOrigin("*")
    @PostMapping("/logout")
    @ResponseBody
    public ResponseEntity logout(@RequestHeader(value = "Authorization") String authHeader) {
        HttpHeaders headers = new HttpHeaders();
        String token = JwtTokenProvider.extractToken(authHeader);
        if (token != null) {
            if (iLoginService.logout(token)) {
                headers.remove("Authorization");
                return new ResponseEntity(headers, HttpStatus.OK);
            }
        }
        return new ResponseEntity(headers, HttpStatus.NOT_MODIFIED);
    }

    /**
     *
     * @param authHeader
     * @return boolean. if request reach here it means it is a valid token.
     */
    @PostMapping("/validToken")
    @ResponseBody
    public Boolean isValidToken(@RequestHeader(value = "Authorization") String authHeader) {
        return true;
    }

    @PostMapping("/refreshToken")
    @CrossOrigin("*")
    @ResponseBody
    public ResponseEntity<AuthResponse> refreshToken(@RequestHeader(value = "Authorization") String authHeader) {
        HttpHeaders headers = new HttpHeaders();
        String token = JwtTokenProvider.extractToken(authHeader);
        if (token == null) {
            return new ResponseEntity<AuthResponse>(new AuthResponse(null), headers, HttpStatus.resolve(400));
        }
        String newToken = iLoginService.refreshToken(token);
        List<String> headerList = new ArrayList<>();
        List<String> exposeList = new ArrayList<>();
        headerList.add("Content-Type");
        headerList.add(" Accept");
        headerList.add("X-Requested-With");
        headerList.add("Authorization");
        headers.setAccessControlAllowHeaders(headerList);
        exposeList.add("Authorization");
        headers.setAccessControlExposeHeaders(exposeList);
        headers.set("Authorization", newToken);
        return new ResponseEntity<AuthResponse>(new AuthResponse(newToken), headers, HttpStatus.CREATED);
    }
}
