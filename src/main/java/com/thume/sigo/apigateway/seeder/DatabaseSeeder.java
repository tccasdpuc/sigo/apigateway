package com.thume.sigo.apigateway.seeder;

import com.thume.sigo.apigateway.bean.auth.Role;
import com.thume.sigo.apigateway.bean.auth.User;
import com.thume.sigo.apigateway.repository.UserRepository;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DatabaseSeeder {

    private Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public DatabaseSeeder(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedUsersTable();
    }

    private void seedUsersTable() {
        ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase("login");
        User admin = new User();
        admin.setLogin("admin");
        Example<User> ex = Example.of(admin, matcher);
        long count = userRepository.count(ex);
        if (count > 0) {
            return;
        }

        admin.setPassword(passwordEncoder.encode("admin"));

        Set<Role> roles = new HashSet<Role>();
        Role adminRole = new Role();
        adminRole.setRole("admin");
        roles.add(adminRole);
        admin.setRole(roles);

        userRepository.save(admin);
        logger.info("Admin User created");
    }
}
