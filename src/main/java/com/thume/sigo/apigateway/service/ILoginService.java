package com.thume.sigo.apigateway.service;

import com.thume.sigo.apigateway.bean.auth.User;

public interface ILoginService {

    String login(String username, String password);

    User saveUser(User user);

    boolean logout(String token);

    Boolean isValidToken(String token);

    String refreshToken(String token);
}
