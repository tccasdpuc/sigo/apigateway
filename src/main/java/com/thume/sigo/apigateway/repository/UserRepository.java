package com.thume.sigo.apigateway.repository;

import com.thume.sigo.apigateway.bean.auth.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    @Query("{ login : ?0}")
    User findByLogin(String login);
}
