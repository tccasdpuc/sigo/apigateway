#
# Build stage
#
FROM maven:3.6.0-jdk-8-slim AS build
COPY src /home/apigateway/src
COPY pom.xml /home/apigateway
RUN mvn -f /home/apigateway/pom.xml clean package -Dmaven.test.skip=true

#
# Package stage
#
FROM openjdk:8-jre-slim
COPY --from=build /home/apigateway/target /usr/local/lib/apigateway
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/apigateway/apigateway-0.0.1.jar"]